<?php namespace App\Http\Controllers;

require_once(app_path().'/TwitterStream.php');
require_once(app_path().'/lib/Phirehose.php');
require_once(app_path().'/lib/OauthPhirehose.php');

use Abraham\TwitterOAuth\TwitterOAuth;
use Illuminate\Support\Facades\Request;

class TwitterController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

  public function feed()
  {
//      $connection = new TwitterOAuth('MaHaYdMEVGiLpV2NyStuwGqtx', 'ChoFTQKj1gORuGfJcjzWfM0Jm6DC19GGZXyCL87pavI4AOgcf8', '374546018-YInloWwoPOsbwbgZ2BMgZn44wzSC1DwcFeaBYf5k', 'XF9gUZHzFOOkrvYShsDECGsEr6XKF6cFksPWEGvYQIczR');
//      $positive = $connection->get("search/tweets", array("q" => "Liverpool :)", "count" => "100"));
//      $negative = $connection->get("search/tweets", array("q" => "Liverpool :("));
//
//      $positiveArr = array();
//
//      foreach($positive->statuses as $tweet) {
//          $item = ['text' => $tweet->text];
//          array_push($positiveArr, $item);
//      }

      // The OAuth credentials you received when registering your app at Twitter
      define("TWITTER_CONSUMER_KEY", "MaHaYdMEVGiLpV2NyStuwGqtx");
      define("TWITTER_CONSUMER_SECRET", "ChoFTQKj1gORuGfJcjzWfM0Jm6DC19GGZXyCL87pavI4AOgcf8");


      // The OAuth data for the twitter account
      define("OAUTH_TOKEN", "374546018-YInloWwoPOsbwbgZ2BMgZn44wzSC1DwcFeaBYf5k");
      define("OAUTH_SECRET", "XF9gUZHzFOOkrvYShsDECGsEr6XKF6cFksPWEGvYQIczR");

      $ts = new \TwitterStream(OAUTH_TOKEN, OAUTH_SECRET, \Phirehose::METHOD_FILTER);
      $ts->setTrack(array('morning', 'goodnight', 'hello', 'the'));
      $ts->consume();

//      return response()->json([
//              'positive' => $positiveArr,
//              'negative' => $negative
//          ]);
  }

    public function search()
    {
        $connection = new TwitterOAuth('MaHaYdMEVGiLpV2NyStuwGqtx', 'ChoFTQKj1gORuGfJcjzWfM0Jm6DC19GGZXyCL87pavI4AOgcf8', '374546018-YInloWwoPOsbwbgZ2BMgZn44wzSC1DwcFeaBYf5k', 'XF9gUZHzFOOkrvYShsDECGsEr6XKF6cFksPWEGvYQIczR');
        $query = Request::input('query'); // Input from form

        $result = $connection->get('search/tweets', array(
            'q' => $query,
            'geocode' => '-2.548926,118.0148634,2300km',
            'count' => 100
        ));

        $datas = array();

        // Take only tweet and location from search result
        foreach($result->statuses as $tweet) {
            if (isset($tweet->place)) {
                array_push($datas, [
                    'tweet' => $tweet->text,
                    'location' => $tweet->place->name
                ]);
            }
        }

        $return = array();

        // Filter tweets by location
        foreach ($datas as $key => $value) {
            $return[$value['location']][] = array(
                'tweet'=>$value['tweet']
            );
        }

        $tweetCount = array();

        // Count tweets by location
        foreach ($return as $key => $value) {
            array_push($tweetCount, [
                'place' => $key,
                'count' => count($value)
            ]);
        }

        return response()->json(['result' => $tweetCount]);
    }

}
